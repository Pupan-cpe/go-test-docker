package main

import (
	"fmt"
	"log"
	"net/http"
	condition_test "testgo/condition"
)

func main() {

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		fmt.Fprintf(w, "Hello Docker")
	})

	log.Fatal(http.ListenAndServe(":8585", nil))

	condition_test.Test()
}